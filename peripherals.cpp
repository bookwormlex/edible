//Peripheral.cpp

#include "peripherals.h"

// Format | C Type         | JavaScript Type   | Size (octets) | Notes
// -------------------------------------------------------------------
//    A   | char[]         | Array             |     Length     |  (1)
//    x   | pad byte       | N/A               |        1       |
//    c   | char           | std::string (length 1) |        1       |  (2)
//    b   | signed char    | number            |        1       |  (3)
//    B   | unsigned char  | number            |        1       |  (3)
//    h   | signed short   | number            |        2       |  (3)
//    H   | unsigned short | number            |        2       |  (3)
//    i   | signed long    | number            |        4       |  (3)
//    I   | unsigned long  | number            |        4       |  (3)
//    l   | signed long    | number            |        4       |  (3)
//    L   | unsigned long  | number            |        4       |  (3)
//    s   | char[]         | string            |     Length     |  (2)
//    f   | float          | number            |        4       |  (4)
//    d   | double         | number            |        8       |  (5)
	
	void exitError(const char* errMsg) 
	{
	  //print to the serial Arduino is attached to, i.e. /dev/ttyGS0 
	  string s_cmd("echo 'error: ");
 	 	s_cmd = s_cmd + errMsg + " - exiting' > /dev/ttyGS0";
 	 	system(s_cmd.c_str());
 	 	exit(EXIT_FAILURE);
	} //exitError()
	
	////// Class Characteristic //////
	
	Characteristic::Characteristic(char* buf) //: addr(buf)
	{
		UUID = addr;  		
		name = addr + MAX_UUID_LEN;  		
		value = name + MAX_NAME_LEN; 	
		property = value + MAX_VAL_LEN;	
		dirtyBit = 0;
	} //Characteristic(buf)
	
	Characteristic::Characteristic()
	{
		UUID = name = value = property = NULL;
		dirtyBit = 0;
	}
	
	char* Characteristic::getCharV()
	{
		return value;
	}
	
	bool Characteristic::upToDate()
	{
		return !dirtyBit;
	} //upToDate()
	
	void Characteristic::setCharV(std::string s)
	{
		memcpy(value, s.c_str(), s.length()); 
	}
	
	
	////// Class Peripheral //////
	
	Peripheral::Peripheral(char* buf) //: addr(buf)
	{
		UUID = addr;  		
		name = addr + MAX_UUID_LEN;  		
		
		for (int i=0; i<MAX_CHAR; i++)
	  {
	  	chars[i] = Characteristic((name + MAX_NAME_LEN)+(i*(CbSize)));
	  } //for each
	} //Peripheral(buf)
	
	Peripheral::Peripheral()
	{
		UUID = name = NULL;
	}
	
	////// Class Buffer //////
	
	Buffer::Buffer(std::string file) 
	{
		fd = open(file.c_str(), O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
		if (fd == -1) exitError("couldn't open mmap file");
		if (ftruncate(fd, (BbSize)) == -1)
			exitError("couldn' modify mmap file");
		Buffer::unpack();
		//buf.close();
	} //constructor
	
	
	void Buffer::unpack()
	{
		char* addr = static_cast<char*>(mmap(NULL, BbSize, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0)); 
	  for (int i=0; i<MAX_PERIPH; i++)
	  {
	  	buffer[i] = Peripheral(addr+(i*(PbSize)));
	  } //for each peripheral
	} //unpack()
	
	
	
	void Buffer::pack()
	{
	}//