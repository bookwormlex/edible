//EdiBLE.cpp

#include "EdiBLE.h"
#include "peripherals.h"
#include <string>


	int CharInd(int periph) 
	{ 
		static int n[MAX_PERIPH]; 
		static bool t=0;
		do {memset(n, -1, MAX_PERIPH*sizeof(int)); t=1;} while (t==0);
		return (n[periph]++);
	}
		
	int PeriphInd()
	{
		static int n=-1;
		return (n++);
	}
			

	//class BLECharacteristic 
	BLECharacteristic::BLECharacteristic(std::string uuid, int properties, int maxLen) : Uuid(uuid), MaxLen(maxLen) {};
	
	void BLECharacteristic::setCharV(std::string s)
	{
		addr->setCharV(s);
	} //BLECharacteristic::setCharV()
	
	std::string BLECharacteristic::getCharV()
	{
		return std::string(addr->getCharV());
	} //BLECharacteristic::getCharV()
	//end class BLECharacteristic
	
	bool BLECharacteristic::written()
	{
		return addr->upToDate();
	}
	
	
	

	//class BLEPeripheralHelper
	BLEPeripheralHelper::BLEPeripheralHelper() //: Peripheral()
	{
		PID = 124; //errorCase //PeriphInd();
	} //BLEPeripheralHelper()
	
	BLEPeripheralHelper::BLEPeripheralHelper(std::string uuid)
	{
		PID = PeriphInd();
	}//BLEPeripheralHelper(uuid)
	
	void BLEPeripheralHelper::setLocalName(std::string n)
	{
		memcpy(addr->name, &n, MAX_NAME_LEN);
	}//BLEPeripheralHelper::setLocalName()

	void BLEPeripheralHelper::setAdvertisedServiceUuid(std::string n)
	{
		memcpy(addr->UUID, &n, MAX_UUID_LEN);
	} //BLEPeripheralHelper::setAdvertisedServiceUuid()
	
	void BLEPeripheralHelper::addAttribute(BLECharacteristic& ch)
	{
		int CtID = CharInd(PID);
		memcpy(addr->chars[CtID].UUID, &(ch.Uuid), sizeof(ch.Uuid));
		//addr->chars[CtID]->name = ch.
		std::stringstream ss; 
		ss<<ch.Properties; 
		std::string pr = ss.str();
		memcpy(addr->chars[CtID].property, &pr, sizeof(pr));
	}
			
	void BLEPeripheralHelper::addAttribute(BLEService& srv)
	{
		srv.perID = PID;
	}
	
	
	//end class BLEPeripheralHelper
	
	
	
	//class BLECentral
	
	BLECentral::BLECentral() : Buf("/tmp/arduino") {};
	
	BLECentral::~BLECentral() {delete &Buf;}
	
	
	
	bool BLECentral::begin()
	{
		//call bash script here
		system("chmod +x BLE.sh"); 
		system("node BLE.sh"); 
	} //BKLECentral::begin()
	
	
	void BLECentral::addAttribute(BLECharacteristic ch)
	{
	
	} //BLEPeripheralHelper::addAttribute()
	
	void BLECentral::addAttribute(BLEService srv)
	{
	} //BLECentral addAttribute()
	
	//end class BLECentral
	
	
	