//Edison library for BLE communications in Arduino IDE

#ifndef _EDIBLE_
	#define _EDIBLE_ 
	
	#include "peripherals.h"
	
	#define BLERead 1
	#define BLEWrite 2
	#define BLENotify 4
	
	#undef min
	#undef max
	
	#include "Arduino.h"
	#include <string>
	#include <sstream>
	#include <cstdlib>
	#include <stdlib.h>
	
	int CharInd(int periph);
	int PeriphInd();
	
	//Arduino classes
	
	
	//global scope, be careful of these.	
	//Buffer Buf("/tmp/arduino"); 

	
	class BLEService 
	{
		public:
			BLEService(std::string UUID) : Uuid(UUID) {};
			std::string uuid() {return Uuid;};
			std::string Uuid;
			
		//private:
			int perID;
	};
	
	class BLECharacteristic //: public Characteristic
	{
		friend class BLEPeripheralHelper;
		public:
			BLECharacteristic(std::string uuid, int properties, int maxLen = 20);
			void setCharV(std::string s);
			std::string getCharV();
			bool written();			
			std::string Uuid;
			int Properties;
			int MaxLen;
		private: 
			Characteristic* addr;
			int CtID;
	};
	
		class BLECentral
	{
		public:
			BLECentral(); //: Buf(new Buffer("/tmp/arduino")) {};
			~BLECentral();
			bool begin();
			void addAttribute(BLECharacteristic ch);
			void addAttribute(BLEService srv);
		
		//private:
			Buffer Buf; 
	};
	
	class BLEBoolCharacteristic : public BLECharacteristic
	{
		public:
		BLEBoolCharacteristic(std::string uuid, int properties, int maxLen = 20);
		void setValue(bool c) {std::stringstream ss; ss<<int(c); setCharV(ss.str());};
		bool value(){return bool(atoi(getCharV().c_str()));};
	};
	
	class BLECharCharacteristic : public BLECharacteristic
	{
		public:
		BLECharCharacteristic(std::string uuid, int properties, int maxLen = 20);
		void setValue(char c) {setCharV(std::string(1, c));};
		char value(){return char(getCharV()[0]);};
	};
	
	class BLEUnsignedCharCharacteristic : public BLECharacteristic
	{
		public:
		BLEUnsignedCharCharacteristic(std::string uuid, int properties, int maxLen = 20);
		void setValue(unsigned char c) {setCharV(std::string(1, char(c)));};
		unsigned char value(){return (unsigned char)(getCharV()[1]);};
	};
	
	class BLEShortCharacteristic : public BLECharacteristic
	{
		public:
		void setValue(short c) {std::stringstream ss; ss<<int(c); setCharV(ss.str());};
		short value(){return short(atoi(getCharV().c_str()));};
	};
	
	class BLEUnsignedShortCharacteristic : public BLECharacteristic
	{
		public:
			void setValue(unsigned short c) {std::stringstream ss; ss<<int(c); setCharV(ss.str());};
			unsigned short value(){return (unsigned short)(atoi(getCharV().c_str()));};
	};
	
	class BLEIntCharacteristic : public BLECharacteristic
	{
		public:
		BLEIntCharacteristic(std::string uuid, int properties, int maxLen = 20) : BLECharacteristic(uuid, properties, maxLen) {};
		void setValue(int c) {std::stringstream ss; ss<<c; setCharV(ss.str());};
		int value(){return int(atoi(getCharV().c_str()));};
	};
	
	class BLEUnsignedIntCharacteristic : public BLECharacteristic
	{
		public:
		void setValue(unsigned int c) {std::stringstream ss; ss<<int(c); setCharV(ss.str());};
		unsigned int value(){return (unsigned int)(atoi(getCharV().c_str()));};
	};
	
	class BLELongCharacteristic : public BLECharacteristic
	{
		public:
		void setValue(long c) {std::stringstream ss; ss<<int(c); setCharV(ss.str());};
		long value(){return long(atol(getCharV().c_str()));};
	};
	
	class BLEUnsignedLongCharacteristic : public BLECharacteristic
	{
		public:
		void setValue(unsigned long c) {std::stringstream ss; ss<<int(c); setCharV(ss.str());};
		unsigned long value(){return (unsigned long)(atol(getCharV().c_str()));};
	};


	class BLEPeripheralHelper //: public Peripheral
	{
		public:
			BLEPeripheralHelper();
			BLEPeripheralHelper(std::string uuid);
			void setLocalName(std::string n);
			void setAdvertisedServiceUuid(std::string n);
			void addAttribute(BLECharacteristic& ch);
			void addAttribute(BLEService& srv);
		private:
			Peripheral* addr;
			int PID;
	};		

//implement other types here
	


	
#endif
	