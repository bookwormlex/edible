//Services header file to define UUID's, etc.
//Replace U1, U2, etc with the variable names and ID's of the services and 
//characteristics you want to subscribe to.

var U1 = '1';
var SERVICE_1_UUID = '2947ac9efc3811e586aa5e5517507c66';
var CHAR_1_UUID = '2947af14fc3811e586aa5e5517507c66';
 
function getLabel(uuid) {
  var label = null;

  if(uuid == U1) 
  	label = 'U1';
  return label;
}	 //getLabel 