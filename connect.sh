#!/bin/bash
pass="$1"


root
pass

rmmod g_multi
mkdir /EdiBLE_scripts
losetup -o 8192 /dev/loop0 /dev/disk/by-partlabel/EdiBLE_scripts
mount /dev/loop0 /EdiBLE_scripts

#update all libraries
cat "src/gz all  http://repo.opkg.net/edison/repo/all \n src/gz edison http://repo.opkg.net/edison/repo/edison \n src/gz core2-32 http://repo.opkg.net/edison/repo/core2-32 \n" >> /etc/opkg/base-feeds.conf 

opkg update
opkg install bluez5-dev
npm install -g async
npm install noble
npm install bleno

#enable bludetooth dev
rfkill unblock bluetooth
systemctl disable bluetooth
hciconfig hci0 up

