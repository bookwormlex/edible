// Peripheral and friend classes for maintaining a buffer binary file
// shared between two processes.  These functions are for the 
// master process.


#ifndef PERIPH_H
	#define PERIPH_H
	
	#include <fstream>
	#include <string>	
<<<<<<< HEAD
	#include <sys/mman.h>
	#include <sys/stat.h>
	#include <sys/types.h>
	#include <fcntl.h>
	#include "Arduino.h"
=======
	#include <thread>
	#include <errno.h>
>>>>>>> e882c65e67e2d1392f96e30135829b0918638a90
	
	#define MAX_UUID_LEN 32  //up to 128
	#define MAX_NAME_LEN 32
	#define MAX_VAL_LEN 20 //BLE Standard, cannot be changed
<<<<<<< HEAD
	#define TYPE 8
	#define MAX_CHAR 10
	#define MAX_PERIPH 10
	#define CHAR_SIZE 8
	
	using namespace std;
	
	void exitError(const char* errMsg);
	
	class Characteristic
=======
	#define TYPE 60
	#define MAX_CHAR 10
	#define MAX_PERIPH 10
	#define CHAR_SIZE 8

  
  
	
	class Char
>>>>>>> e882c65e67e2d1392f96e30135829b0918638a90
	{
		friend class Peripheral;
		friend class Buffer;
		
		public:
<<<<<<< HEAD
			Characteristic(char* buf);
			Characteristic();
			//~Characteristic();
			bool upToDate();
			char* getCharV();
			void setCharV(std::string s);
			
		//protected:	
			char *addr; // = NULL; //beginning of congruent address in buffer
			void unpack();
			#define CbSize (CHAR_SIZE * ( MAX_UUID_LEN + MAX_NAME_LEN + MAX_VAL_LEN + TYPE + 1))
			
		//private:         				
			char* UUID; //= NULL;		
			char* name; //= NULL;		
			char* value; //= NULL;	
			char* property; //= NULL;
			bool dirtyBit; // = 0; //is 1 if characteristic needs to be updated
			
			
	}; //class Characteristic
=======
			Char(char* buf);
			~Char();
			bool upToDate();
			
		protected:	
			char **addr = NULL; //beginning of congruent address in buffer
			void unpack();
			const int bsize = CHAR_SIZE * ( MAX_UUID_LEN + MAX_NAME_LEN + 
													MAX_VAL_LEN + TYPE + 1);
			
		private:         				
			char** UUID = NULL;		
			char** name = NULL;		
			char** value = NULL;	
			char** property = NULL;
			bool dirtyBit = 0; //is 1 if characteristic needs to be updated
			
			
	}; //class Char
>>>>>>> e882c65e67e2d1392f96e30135829b0918638a90
	
	
	
	class Peripheral
	{
		friend class Buffer;
		
		public:
			Peripheral();
<<<<<<< HEAD
			Peripheral(char* buf);
			//~Peripheral();
			
		//protected: 
			char* addr; //= NULL;
			//void unpack();
			#define PbSize (CbSize * MAX_CHAR)
			
		//private:
			char* UUID; 
			char* name;
			Characteristic chars[MAX_CHAR];
=======
			~Peripheral();
			
		protected: 
			char** addr = NULL;
			void unpack();
			const int bsize = Char.bsize * MAX_CHAR;
			
		private:
			char** UUID; 
			char** name;
			Char chars[MAX_CHAR];
>>>>>>> e882c65e67e2d1392f96e30135829b0918638a90
	}; //class Peripheral
	
	
	
	class Buffer
	{
	  public:
<<<<<<< HEAD
	  	//Buffer();
	  	Buffer(std::string file);
	  	//~Buffer();
	  	//void update();
	  	void beginBLE();
	  	
	  //protected:
	  	#define BbSize (PbSize * MAX_PERIPH)
	  	//std::fstream buf; //address
	  	int fd; //file descriptor
	  	
	  //private:
=======
	  	Buffer();
	  	Buffer(String file);
	  	~Buffer();
	  	addPeripheral(string uuid, string m);
	  	addCharacteristic(string uuid, string n, string prop)
	  	typedef <template t> setCharacteristic( 
	  	void update();
	  	void beginBLE();
	  	
	  protected:
	  	const int bSize = Peripheral.bSize * MAX_PERIPH;
	  	std::fstream buf; //address
	  	bool fileRequest();
	  	
	  	
	  private:
>>>>>>> e882c65e67e2d1392f96e30135829b0918638a90
	  	Peripheral buffer[MAX_PERIPH];
	  	
	  	void pack();
	  	void unpack();
	  	void update();
	}; //class Buffer
	
	
<<<<<<< HEAD
=======
	
>>>>>>> e882c65e67e2d1392f96e30135829b0918638a90
// Format | C Type         | JavaScript Type   | Size (octets) | Notes
// -------------------------------------------------------------------
//    A   | char[]         | Array             |     Length     |  (1)
//    x   | pad byte       | N/A               |        1       |
<<<<<<< HEAD
//    c   | char           | std::string (length 1) |        1       |  (2)
=======
//    c   | char           | string (length 1) |        1       |  (2)
>>>>>>> e882c65e67e2d1392f96e30135829b0918638a90
//    b   | signed char    | number            |        1       |  (3)
//    B   | unsigned char  | number            |        1       |  (3)
//    h   | signed short   | number            |        2       |  (3)
//    H   | unsigned short | number            |        2       |  (3)
//    i   | signed long    | number            |        4       |  (3)
//    I   | unsigned long  | number            |        4       |  (3)
//    l   | signed long    | number            |        4       |  (3)
//    L   | unsigned long  | number            |        4       |  (3)
//    s   | char[]         | string            |     Length     |  (2)
//    f   | float          | number            |        4       |  (4)
//    d   | double         | number            |        8       |  (5)
	
	
<<<<<<< HEAD
=======

	////// Class Char //////
	
	//public
	Char::Char(char* buf) : addr(buf)
	{
		UUID = addr;  		
		name = addr + MAX_NAME_LEN;  		
		value = name + MAX_NAME_LEN; 	
		property = value + MAX_VAL_LEN;	
	} //Char()
	
	
	bool Char::upToDate()
	{
		return !dirtyBit;
	} //upToDate()
	
	//protected
	void Char::unpack()
	{
	  //is this function even necessary?
	} //Char::unpack()
	
	
	
	////// Class Peripheral //////
	
	
	
	////// Class Buffer //////
	
	Buffer::Buffer(string file) : 
	{
		buf.open(file, std::fstream::in |std::fstream::out | 
							std::fstream::binary)
		buf.close();
	} //constructor
	
	
	Buffer::unpack()
	{
		
	
	} //unpack()
	
	
	Buffer::pack()
	{
	}//
	
	
	bool Buffer::fileRequest()
	{
		buf.open(file, std::fstream::in | std::fstream::out | 
							std::fstream::binary);
		if (data.fail()) 
    { 
      switch (errno) 
      { 
        case EACCES:
        case EBUSY:
        case ETXTBSY:
        	// this is set if the drive is not ready 
        	//cout << "Drive not ready or permission denied" << endl; 
        	std::this_thread::sleep_for (std::chrono::seconds());
        	break; 
        default: 
          //Add code here
      } //switch errno
    } //if fail()
	} //fileRequest()
	
	bool Buffer::mutexInit()
	{
		pthread_mutexattr_t mutexattr;
  	if (pthread_mutexattr_init(&mutexattr) == -1)
  		exitError("pthread_mutexattr_init");
  	if (pthread_mutexattr_setrobust(&mutexattr, PTHREAD_MUTEX_ROBUST) == -1)
  		exitError("pthread_mutexattr_setrobust");
  	if (pthread_mutexattr_setpshared(&mutexattr, PTHREAD_PROCESS_SHARED) == -1)
  		exitError("pthread_mutexattr_setpshared");
  	if (pthread_mutex_init(&(p_mmapData->mutex), &mutexattr) == -1)
  		exitError("pthread_mutex_init");
	}
	
>>>>>>> e882c65e67e2d1392f96e30135829b0918638a90
#endif